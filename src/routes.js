const express = require('express')

const routes = express.Router()

const controllers = require('./app/controllers/')

const authMiddleware = require('./app/middlewares/auth')

let api_version = 'v1'

routes.post('/c_enterprise', controllers.EnterpriseController.store)

routes.post(
  `/api/${api_version}/users/auth/sign_in`,
  controllers.UserController.store
)

routes.use(authMiddleware)

routes.get(
  `/api/${api_version}/enterprises`,
  controllers.EnterpriseController.index
)

routes.get(
  `/api/${api_version}/enterprises/:id`,
  controllers.EnterpriseController.index
)

module.exports = routes
