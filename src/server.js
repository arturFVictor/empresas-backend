const express = require('express')
const mongoose = require('mongoose')

const dbConfig = require('./config/databaseConfig')

class App {
  constructor () {
    this.express = express()

    this.middlewares()
    this.routes()
    this.database()
  }

  middlewares () {
    this.express.use(express.json())
  }

  routes () {
    this.express.use(require('./routes'))
  }

  database () {
    mongoose.connect(dbConfig.uri, {
      useNewUrlParser: true
    })
  }
}

module.exports = new App().express
