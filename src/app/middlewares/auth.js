const jwt = require('jsonwebtoken')
const dotenv = require('dotenv').config()

module.exports = async (req, res, next) => {
  const { client, uid, ['access-token']: token } = req.headers

  if (!client && !uid && !access-token) {
    return res.status(400).json({ error: 'Token not provided' })
  }

  if(client == process.env.APP_SECRET){
    try {
      const decoded = await jwt.verify(token, process.env.APP_SECRET)

      req.userId = decoded.id

      return next()
    } catch (err) {
      return res.json(401).json({ error: 'Authorized users only!' })
    }
  }
}
