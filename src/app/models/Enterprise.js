const mongoose = require('mongoose')
const autoIncrement = require('mongoose-sequence')(mongoose);

const EnterpriseSchema = new mongoose.Schema({
  _id: {
    type: Number
  },
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  telephone: {
    type: Number,
    require: true
  },
  type: {
    type: Number,
    required: true
  }
},{ _id: false });

EnterpriseSchema.plugin(autoIncrement, {inc_field: '_id'});

module.exports = mongoose.model('Enterprise', EnterpriseSchema)
