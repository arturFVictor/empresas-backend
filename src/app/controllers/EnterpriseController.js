const Enterprise = require('../models/Enterprise')

class EnterpriseController {
  async index (req, res) {
    if(req.params.id){
      const { id } = req.params

      let enterprise = await Enterprise.findOne({_id: id})

      return res.json(enterprise)
    }   
    
    let filters = {}

    if (req.query.name) {
      filters.name = req.query.name
    }
    if (req.query.enterprise_types) {
      filters.type = req.query.enterprise_types
    }

    let enterprises = await Enterprise.find(filters)

    return res.json(enterprises)
  }

  async store (req, res) {
    const { email } = req.body

    if (await Enterprise.findOne({ email })) {
      return res.status(400).json({ error: 'User already exists' })
    }

    const enterprise = await Enterprise.create(req.body)

    return res.json(enterprise)
  }
}

module.exports = new EnterpriseController()
